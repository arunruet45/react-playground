import React from 'react';
import {connect} from "react-redux";
import {buyCake, buyIceCream, chooseProduct} from "../../redux";

const CommonContainer = (props) => {
    const {numOfItem, isCakeChosen, isIceCreamChosen, item} = props;
    const {name} = item;

    const getConditionalColor = (isChosen) => {
        if (isChosen) {
            return {backgroundColor: 'green'}
        }
    }

    return (
        <div>
            <h2>Common Container {name} - {numOfItem}</h2>
            <button onClick={props.buyCake}>Buy {props.item.name}</button>
            <div>
                <button style={getConditionalColor(isCakeChosen)} onClick={() => props.chooseProduct('cake')}>
                    {!isCakeChosen && (
                        <>
                            Choose Cake
                        </>
                    )}
                    {isCakeChosen && (
                        <>
                            Cake Chosen
                        </>
                    )}
                </button>
                <button style={getConditionalColor(isIceCreamChosen)} onClick={() => props.chooseProduct('iceCream')}>
                    {!isIceCreamChosen && (
                        <>
                            Choose IceCream
                        </>
                    )}
                    {isIceCreamChosen && (
                        <>
                            IceCream Chosen
                        </>
                    )}
                </button>
            </div>
        </div>
    )
};

const mapStateToProps = (state, ownProps) => {
    const itemState = ownProps.item.value === 'cake' ? state.cake.numOfCakes : state.iceCream.numOfIceCream;
    return {
        numOfItem: itemState,
        isCakeChosen: state.product.isCakeChosen,
        isIceCreamChosen: state.product.isIceCreamChosen
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        buyCake: ownProps.item.value === 'cake' ? () => dispatch(buyCake()) : () => dispatch(buyIceCream()),
        chooseProduct: (productKey) => dispatch(chooseProduct(productKey)),
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CommonContainer);