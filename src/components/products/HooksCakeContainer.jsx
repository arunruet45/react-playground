import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {buyCake} from "../../redux";

const HooksCakeContainer = () => {
    const numOfCakes = useSelector(state => state?.['cake']?.numOfCakes);
    const dispatch = useDispatch();
    const [cakeQuantity, setCakeQuantity] = useState(1);

    const onChangeCakeQuantity = (e) => {
        const quantity = e.target.value;
        setCakeQuantity(quantity);
    }
    return (
        <div>
            <h2>Hooks No of cakes - {numOfCakes}</h2>
            <input type='text' value={cakeQuantity} onChange={onChangeCakeQuantity}/>
            <button onClick={() => dispatch(buyCake(cakeQuantity))}>Buy {cakeQuantity} Cake</button>
        </div>
    )
};

export default HooksCakeContainer;