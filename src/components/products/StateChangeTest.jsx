import React from 'react';
import {connect} from "react-redux";
import {mapStateToProps} from "../../redux/Utils";

const StateChangeTest = (props) => {
    return (
        <div>
            <h2>State change test - {props.cake.numOfCakes}</h2>
        </div>
    )
};

// const mapStateToProps = (state) => {
//     return {
//         numOfCakes: state.cake.numOfCakes
//     }
// }

export default connect(
    mapStateToProps,
)(StateChangeTest);