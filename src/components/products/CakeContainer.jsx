import React from 'react';
import {connect} from "react-redux";
import {buyCake} from "../../redux";
import {mapStateToProps} from "../../redux/Utils";

const CakeContainer = (props) => {
    return (
        <div>
            <h2>No of cakes - {props.cake.numOfCakes}</h2>
            <button onClick={props.buyCake}>Buy Cake</button>
        </div>
    )
};

const mapDispatchToProps = (dispatch) => {
    return {
        buyCake: () => dispatch(buyCake())
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CakeContainer);