import React from 'react';
import {connect} from "react-redux";
import {buyIceCream} from "../../redux";
import {mapStateToProps} from "../../redux/Utils";

const IceCreamContainer = (props) => {

    return (
        <div>
            <h2>No of Ice cream - {props.iceCream.numOfIceCream}</h2>
            <button onClick={props.buyIceCream}>Buy Icecream</button>
        </div>
    )
};

const mapDispatchToProps = (dispatch) => {
    return {
        buyIceCream: () => dispatch(buyIceCream())
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(IceCreamContainer);