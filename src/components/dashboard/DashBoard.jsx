import React from "react";
import {connect} from "react-redux";
import CakeContainer from "../products/CakeContainer";
import HooksCakeContainer from "../products/HooksCakeContainer";
import StateChangeTest from "../products/StateChangeTest";
import IceCreamContainer from "../products/IceCreamContainer";
import CommonContainer from "../products/CommonContainer";
import {mapStateToProps} from "../../redux/Utils";
import User from "../user/User";

const DashBoard = (props) => {
    const {isCakeChosen} = props.product
    const item = isCakeChosen ? {value: 'cake', name: 'Cake'} : {value: 'iceCream', name: 'IceCream'}
    return (
        <>
            <CakeContainer/>
            <HooksCakeContainer/>
            <StateChangeTest/>
            <IceCreamContainer/>
            <CommonContainer item={item}/>
            <User/>
        </>
    )
}

export default connect(mapStateToProps)(DashBoard);