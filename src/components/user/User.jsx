import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {fetchUserFailure, fetchUserRequest, fetchUserSuccess} from "../../redux";
import userService from "../../services/UserService";

const User = () => {
    const data = useSelector(state => state?.['user']);
    const {users} = data;
    const dispatch = useDispatch();

    const fetchUsers = async () => {
        dispatch(fetchUserRequest());
        const response = await userService.fetchUsers();
        const {data, success, error} = response;

        if (!success) {
            dispatch(fetchUserFailure(error));
            return
        }

        dispatch(fetchUserSuccess(data));
    }

    const onClickFetchUser = async () => {
        await fetchUsers();
    }

    return (
        <div>
            <button type="button" onClick={onClickFetchUser}>Fetch User</button>
            <div>
                {users?.length > 0 && (
                    <>
                        {users.map((item) => {
                            return (
                                <>
                                    <span key={item.id}>{item.name}</span>
                                    <br/>
                                </>
                            )
                        })}
                    </>
                )}
            </div>
        </div>
    )
}

export default User;