import {CHOOSE_PRODUCT} from "./ProductTypes";

const initialState = {
    isCakeChosen: true,
    isIceCreamChosen: false
}

const ProductReducer = (state = initialState, action) => {
    switch (action.type) {
        case CHOOSE_PRODUCT:
            return {
                ...state,
                isIceCreamChosen: action.payload === 'iceCream',
                isCakeChosen: action.payload === 'cake',
            }
        default:
            return state
    }
}

export default ProductReducer;