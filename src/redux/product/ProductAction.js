import {CHOOSE_PRODUCT} from './ProductTypes';

export const chooseProduct = (productKey) => {
    return {
        type: CHOOSE_PRODUCT,
        payload: productKey
    }
}