import {combineReducers} from "redux";
import CakeReducer from "./cakes/CakeReducer";
import IceCreamReducer from "./ice-cream/IceCreamReducer";
import ProductReducer from "./product/ProductReducer";
import UserReducer from "./user/UserReducer";

const RootReducer = combineReducers({
    cake: CakeReducer,
    iceCream: IceCreamReducer,
    product: ProductReducer,
    user: UserReducer
})

export default RootReducer