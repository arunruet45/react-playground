import {BUY_CAKE} from './CakeTypes';

export const buyCake = (quantity = 1) => {
    return {
        type: BUY_CAKE,
        payload: quantity
    }
}