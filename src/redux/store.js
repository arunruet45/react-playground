import {createStore, applyMiddleware} from "redux";
import logger from 'redux-logger';
import {composeWithDevTools} from "redux-devtools-extension";
import RootReducer from "./RootReducer";
import thunk from "redux-thunk";

const Store = createStore(RootReducer, composeWithDevTools(applyMiddleware(logger, thunk)));

export default Store;