import axios from "axios";
import {error, success} from "./ResponseHandler";

class UserService {
    async fetchUsers () {
        try {
            const response = await axios.get('https://jsonplaceholder.typicode.com/users');
            return success(response);
        } catch (e) {
            return error(e);
        }
    }
}

export default new UserService();