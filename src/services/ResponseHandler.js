const success = (response) => {
    return {
        data: response?.data,
        success: true
    };
};

const error = (response) => {
    return {
        data: {},
        success: false,
        error: response["error"]
    };
};

export {success, error};