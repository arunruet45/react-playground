import './App.css';

import React from "react";
import {Provider} from "react-redux";
import Store from "./redux/store";
import DashBoard from "./components/dashboard/DashBoard";

function App() {
    return (
        <Provider store={Store}>
            <div className="App">
                <DashBoard/>
            </div>
        </Provider>
    );
}

export default App;
